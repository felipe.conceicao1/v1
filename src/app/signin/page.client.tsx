"use client";
import { SignInSchema, signInSchema } from "@/utils/schemas";
import { Button, TextField } from "@mui/material";
import { SubmitHandler, useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { SignInForm } from "@/components/sign-in/sign-in-form";

export function SignInScreen() {
  return (
    <div className=" xl:max-w-[1600px] ml-auto h-screen flex items-center xl:px-20 px-8">
      <SignInForm />
    </div>
  );
}
