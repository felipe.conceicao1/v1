import UploadFilesScreen from "./uploadfiles.client";

async function getData() {
  try {
    const res = await fetch("https://back.boostlabs.ai/api/v1/files", {
      cache: "no-store",
    });
    const json = await res.json();
    return json;
  } catch (err) {
    console.error(err);
  }
}

export default async function UploadFiles() {
  const data = await getData();

  return (
    <div className="flex flex-1 bg-oceanBlue xl:flex-row flex-col">
      <div className="text-white xl:px-3 px-8 bg-deepBlue xl:w-56 border-r-[2.8px] border-[#13131386]">
        <h1 className=" p-3  rounded-md font-medium text-2xl xl:mt-8">
          Vendedor
        </h1>
        <div className="flex xl:mt-16 my-16">
          <div className="bg-vibrantGreen rounded-l-xl h-4 w-[30%] relative">
            <span className="text-sm absolute top-5">30Mb</span>
          </div>
          <div className="bg-white rounded-r-xl h-4 w-[70%] relative">
            <span className="text-sm absolute bottom-5 right-0 text-lightGray">
              100Mb
            </span>
          </div>
        </div>
      </div>
      <UploadFilesScreen data={data} />
    </div>
  );
}
