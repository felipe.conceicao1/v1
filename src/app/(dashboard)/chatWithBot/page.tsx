"use client";
import { ChatWithBotTopNav } from "@/components/dashboard/chat-with-bot/chat-with-bot-topnav";
import { ChatWithBotInput } from "@/components/dashboard/chat-with-bot/chat-with-bot-input";
import { ChatWithBotUserMessageBubble } from "@/components/dashboard/chat-with-bot/chat-with-bot-user-message-bubble";
import { ChatWithBotBotMessageBubble } from "@/components/dashboard/chat-with-bot/chat-with-bot-bot-message-bubble";
import { useState } from "react";

export default function ChatWithBot() {
  const [textBotUser, setTextBotUser] = useState<any>([]);

  console.log("messageData", textBotUser);

  return (
    <div className="flex flex-1 flex-col w-full m-4 h-[100vh]">
      <div className="flex flex-1 overflow-y-hidden">
        <div className="w-full h-full bg-[#002F3C] ">
          <ChatWithBotTopNav chatTitle={"Nova conversa"} />
          <div className="px-10 mt-5 flex flex-col gap-2.5 overflow-y-auto h-full">
            {textBotUser.map((messageData: any, i: number) => {
              if (messageData.robot) {
                return (
                  <ChatWithBotBotMessageBubble
                    key={i}
                    messageData={messageData.robot}
                  />
                );
              } else {
                return (
                  <ChatWithBotUserMessageBubble
                    key={i}
                    messageData={messageData.user}
                  />
                );
              }
            })}
          </div>
        </div>
      </div>
      <ChatWithBotInput setTextBotUser={setTextBotUser} />
    </div>
  );
}
