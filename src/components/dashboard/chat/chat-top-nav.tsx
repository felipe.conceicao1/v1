"use client";;
import { FeelingEmoji } from "@/components/ui/feeling-emoji";
import { Tags } from "@/components/ui/tags";
import { InputAdornment, TextField } from "@mui/material";
import Image from "next/image";
import { useState } from "react";
import { BsPinAngleFill } from "react-icons/bs";
import { FiSearch } from "react-icons/fi";
import { IoMdArrowRoundBack } from "react-icons/io";
import AnimateHeight, { Height } from "react-animate-height";
import Link from "next/link";
import { SlOptionsVertical } from "react-icons/sl";

export function ChatTopNav() {
  const [height, setHeight] = useState<Height>(0);

  return (
    <div className="bg-deepBlue w-full flex flex-col rounded-md h-fit pb-3">
      <div className="flex flex-1  p-[26px] pb-3  items-center justify-between">
        <div className="flex items-center">
          <Link href="/" className="flex xl:hidden pr-2">
            <IoMdArrowRoundBack className="text-white text-4xl" />
          </Link>
          <div className="h-[70px] w-[70px] relative">
            <Image
              src={"/assets/dashboard/random-man.jpg"}
              alt={"Robson Silva"}
              fill
              objectFit="cover"
              className="rounded-full"
            />
          </div>
          <div className="flex flex-col ml-8">
            <h2 className="text-xl font-bold text-white">Robson Silva</h2>
            <Tags className="text-sm">
              <span>TM Alto</span>
            </Tags>
            <span className="text-lightGray">Online</span>
          </div>
        </div>

        <div className="flex items-center  ml-3">
          <button
            aria-expanded={height !== 0}
            onClick={() => setHeight(height === 0 ? "auto" : 0)}
          >
            <SlOptionsVertical className="text-gray-500 text-3xl text-white" />
          </button>
        </div>
      </div>
      <AnimateHeight duration={500} height={height}>
        <div className="flex justify-end mx-2 gap-3 items-center">
          
          <TextField
            variant="filled"
            label="Pesquisar"
            type="text"
            className="h-fit max-w-[260px] min-w-[200px] rounded-lg"
            fullWidth
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <FiSearch className="text-gray-500" />
                </InputAdornment>
              ),
            }}
          /><BsPinAngleFill size={40} color="2C57AB" />
          <FeelingEmoji className="text-[40px]">bad</FeelingEmoji>
        </div>
      </AnimateHeight>
    </div>
  );
}
