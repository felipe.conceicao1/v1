import {Avatar} from '@mui/material';
import Image from 'next/image';
import {ChatAnalysis} from '@/components/dashboard/chat/chat-analysis';

export const ChatMessage = ({message}: any) => {
  return (
    <div key={message.id}
         className={`my-2 flex flex-col ${message.user.isSender ? 'items-end' : 'items-start'}`}
    >
      <div className={`flex items-end ${message.user.isSender ? 'flex-row' : 'flex-row-reverse'}`}>
        <div className='flex flex-col items-start'>
          {message.isNegative && <ChatAnalysis />}
          <div className={`rounded p-4 ${message.user.isBooster ? 'bg-[#C76363] text-white' : (message.user.isSender ? 'bg-[#d7ddeb]' : 'bg-[#D9D9D9]')}`}>
            <div className=''>
              <div className='flex justify-between items-center'>
                <span className='text-sm'>{message.user.name}</span>
                <span className='text-sm'>{message.date}</span>
              </div>
              <div className='text-base mt-2'>{message.message}</div>
            </div>
          </div>
        </div>
        <Avatar className={`${message.user.isSender ? 'ml-2' : 'mr-2'}`}>
          <Image
            src={message.user.avatar} alt={message.user.name} width='80' height='80'
          />
        </Avatar>
      </div>
    </div>
  );
}
