"use client"

import { Tags } from "@/components/ui/tags"
import { BiPlus } from "react-icons/bi"

export const ChatWithBotSidebarTags = () => {
    return (
        <nav className="mt-8 flex flex-col gap-4 px-4">
            <h2 className="font-medium text-lg ">Marcadores</h2>
            <div className="flex items-center">
                <div className="flex gap-4 ">
                    <BiPlus size={24} />
                    <span>Novo</span>
                </div>
            </div>
            <Tags onClick={() => console.log("Tag Clicked!")}>Premium</Tags>
            <Tags onClick={() => console.log("Tag Clicked!")}>Ativo</Tags>
            <Tags onClick={() => console.log("Tag Clicked!")}>TM Alto</Tags>
        </nav>
    )
}