import { TextField, InputAdornment, Button } from '@mui/material'
import React from 'react'
import { FiSearch } from 'react-icons/fi'
import { ChatWithBotSummaryCard } from './chat-with-bot-summary-card'

import Link from 'next/link';
import { ChatWithBotListSearchBar } from './chat-with-bot-list-searchbar';

const mockedChatArray = [{
    chatTitle: 'Título da conversa',
    lastMessage: 'Um X para indicar que o usuário pode cancelar uma ação no ...',
    chatTime: '10:45',
    tags: ['Treinamentos'],
    isActive: true,
    isPinned: true,
    href: '',
    id: 'dhauiehdiua',
},
{
    chatTitle: 'Título do card',
    lastMessage: 'Um X para indicar que o usuário pode cancelar uma ação no ...',
    chatTime: '09:30',
    tags: ['Treinamentos'],
    isActive: false,
    isPinned: true,
    href: '',
    id: 'dhauie123hdiua'
},
{
    chatTitle: 'Título do card 2',
    lastMessage: 'Um X para indicar que o usuário pode cancelar uma ação no ...',
    chatTime: 'ontem',
    tags: ['Produtos'],
    isActive: false,
    isPinned: false,
    href: '',
    id: 'dh6345auiehdiua'
},
{
    chatTitle: 'Título do card 3',
    lastMessage: 'Um X para indicar que o usuário pode cancelar uma ação no ...',
    chatTime: 'segunda-feira',
    tags: ['Produtos'],
    isActive: false,
    isPinned: false,
    href: '',
    id: '123dhauiehdiua'
},
{
    chatTitle: 'Título do card 6',
    lastMessage: 'Um X para indicar que o usuário pode cancelar uma ação no ...',
    chatTime: '03/12/23',
    tags: ['Vendas'],
    isActive: false,
    isPinned: false,
    href: '',
    id: 'dhauiehdiua123'
}]

const ChatWithBotList = () => {
    return (
        <div className="pt-6 bg-deepBlue h-full xl:flex flex-col border-r-[3px] border-r-[#13131386] flex-1 min-w-[456px]">
            <ChatWithBotListSearchBar />
            <div className="text-white flex mt-10 flex-1">
                <section className="w-full overflow-y-auto">
                    {mockedChatArray.map((chatInfo, index) =>
                        <Link href={`/chatWithBot/${chatInfo.href}`} key={chatInfo.id + index}>
                            <ChatWithBotSummaryCard
                                chatTitle={chatInfo.chatTitle}
                                lastMessage={chatInfo.lastMessage}
                                chatTime={chatInfo.chatTime}
                                tags={chatInfo.tags}
                                isActive={chatInfo.isActive}
                                isPinned={chatInfo.isPinned}
                                id={chatInfo.id}
                            />
                        </Link>
                    )}
                </section>
            </div>
        </div>
    )
}

export default ChatWithBotList