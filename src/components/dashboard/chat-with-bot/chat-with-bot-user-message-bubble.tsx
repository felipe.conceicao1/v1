import { Avatar } from "@mui/material";
import Image from "next/image";

export const ChatWithBotUserMessageBubble = ({
  messageData,
}: {
  messageData: any;
}) => {
  return (
    <div key={messageData.date} className="my-2 flex flex-col items-end pl-32">
      <div className="flex items-end flex-row">
        <div className="flex flex-col items-start">
          <div className="rounded p-4 bg-[#d7ddeb] text-black">
            <div className="flex justify-between items-center gap-4">
              <span className="text-sm">{messageData.name}</span>
              <span className="text-sm">{messageData.date.toISOString().substr(11, 8)}</span>
            </div>
            <div className="text-base mt-2">{messageData.text}</div>
          </div>
        </div>
        <Avatar className="ml-2">
          <Image
            src={messageData.imgUrl}
            alt={`${messageData.name} photo`}
            width="80"
            height="80"
          />
        </Avatar>
      </div>
    </div>
  );
};
