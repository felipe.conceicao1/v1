import { BsPinAngle } from "react-icons/bs";
import VolumeUpOutlinedIcon from '@mui/icons-material/VolumeUpOutlined';
import LocalOfferOutlinedIcon from '@mui/icons-material/LocalOfferOutlined';


export const ChatWithBotTopNav = ({chatTitle}: {chatTitle:string}) => {
    return (
        <div className="bg-[#002935] w-full flex flex-col rounded-md h-fit pb-3">
            <div className="flex flex-1  p-[26px] pb-3  items-center justify-between">
                <h2 className="text-xl font-bold text-white">{chatTitle}</h2>
                <div className="flex justify-end mx-2 gap-3 items-center">
                    <VolumeUpOutlinedIcon className="size-10 text-white" />
                    <LocalOfferOutlinedIcon className="size-10 text-white rotate-90" />
                    <BsPinAngle className="size-10 text-white" />
                </div>
            </div>
        </div>
    )
}