"use client";
import Link from "next/link";
import { AiOutlineCalendar } from "react-icons/ai";
import { BiPlus } from "react-icons/bi";
import {
  BsArrowLeftCircleFill,
  BsArrowRightCircleFill,
  BsChatText,
  BsWhatsapp,
} from "react-icons/bs";
import { FiMail } from "react-icons/fi";
import { GoMention } from "react-icons/go";
import { VscCommentUnresolved } from "react-icons/vsc";
import { Tags } from "../ui/tags";
import { useState } from "react";

export function RightSideNavbar() {
  const [width, setWidth] = useState(false);

  return (
    <div className="relative  ">
      <div className="bg-deepBlue flex flex-col xl:static absolute py-8  p-0 z-10 h-full border-r-[3px] border-[#13131386]">
        <div
          className={` text-white  flex flex-col ${
            width ? "w-64" : "w-0 overflow-hidden"
          } xl:w-64 transition-all duration-200 ease-in-out h-full z-10`}
        >
          <button
            onClick={() => setWidth(!width)}
            className="flex xl:hidden absolute -right-6 top-1/2"
          >
            {width ? (
              <BsArrowLeftCircleFill className="text-5xl text-white bg-deepBlue rounded-full" />
            ) : (
              <BsArrowRightCircleFill className="text-5xl text-white bg-deepBlue rounded-full" />
            )}
          </button>

          <nav className="flex flex-col gap-4 px-4">
            <Link href="/">
              <div className="bg-mediumGray p-2 -ml-2 rounded-md flex gap-3">
                <BsChatText size={20} />
                <span>Todas as mensagens</span>
              </div>
            </Link>
            <div className="flex items-center justify-between ">
              <div className="flex gap-4">
                <GoMention size={20} />
                <span>Menções</span>
              </div>
              5
            </div>
            <div className="flex items-center justify-between ">
              <div className="flex gap-4">
                <VscCommentUnresolved size={20} />
                <span>Não lidas</span>
              </div>
              5
            </div>
          </nav>
          <nav className="mt-8 flex gap-4 flex-col px-4">
            <h2 className="font-medium text-lg">Caixas de entrada</h2>
            <div className="flex items-center justify-between ">
              <div className="flex gap-4">
                <BiPlus size={24} />
                <span>Novo</span>
              </div>
              5
            </div>
            <div className="flex items-center justify-between ">
              <div className="flex gap-4">
                <BsWhatsapp size={20} />
                <span>Whatsapp</span>
              </div>
              <span>4</span>
            </div>
            <div className="flex items-center justify-between ">
              <div className="flex gap-4">
                <AiOutlineCalendar size={20} />
                <span>Reuniões</span>
              </div>
              <span>34</span>
            </div>
            <div className="flex items-center justify-between ">
              <div className="flex gap-4">
                <FiMail size={20} />
                <span>E-mail</span>
              </div>
              <span>54</span>
            </div>
          </nav>
          <nav className="mt-8 flex flex-col gap-4 px-4">
            <h2 className="font-medium text-lg">Marcadores</h2>
            <div className="flex items-center">
              <div className="flex gap-4">
                <BiPlus size={24} />
                <span>Novo</span>
              </div>
            </div>
            <Tags onClick={() => console.log("clicked")}>Premium</Tags>
            <Tags onClick={() => console.log("clicked")}>Ativo</Tags>
            <Tags onClick={() => console.log("clicked")}>TM Alto</Tags>
          </nav>
        </div>
      </div>
    </div>
  );
}
