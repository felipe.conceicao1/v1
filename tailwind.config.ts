import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    colors: {
      deepBlue: "#002935",
      darkTeal: "#002F3C",
      midnightBlue: "#013D4F",
      oceanBlue: "#023D4F",
      vibrantGreen: "#50BE50",
      mediumGray: "#5D5D5D",
      lightGray: "#999999",
      softRed: "#C76363",
      silver: "#D9D9D9",
      neonYellow: "#E1F714",
      lavender: "#EADDFF",
      white: "#FFFFFF",
      black: "#000000"
    },
    extend: {},
  },
  plugins: [],
};
export default config;
